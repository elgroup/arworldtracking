/*
 See LICENSE folder for this sample’s licensing information.
 
 Abstract:
 Main view controller for the AR experience.
 */

import UIKit
import SceneKit
import ARKit
import CoreData
import MapKit
import CoreLocation


class ViewController: UIViewController, ARSCNViewDelegate, ARSessionDelegate,CLLocationManagerDelegate {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var sessionInfoView: UIView!
    @IBOutlet weak var sessionInfoLabel: UILabel!
    @IBOutlet weak var sceneView: ARSCNView!
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var stopButton: UIButton!
    var isStart = false
    var appDelegate: AppDelegate!
    var extentz:CGFloat = 0.0
    var centerz:CGFloat = 0.0
    var anchortoSave: ARAnchor!
    
    @IBOutlet weak var coordinatesTextView: UITextView!
    @IBOutlet weak var coordinatesLabel: UILabel!
    var locManager = CLLocationManager()
    var currentLocation: CLLocation!
    var arrCoordinates: NSMutableArray!
    // MARK: - View Life Cycle
    
    /// - Tag: StartARSession
    
    override func viewDidLoad() {
        super.viewDidLoad()
        appDelegate  = UIApplication.shared.delegate as! AppDelegate
        stopButton.isUserInteractionEnabled = false
        locManager.requestWhenInUseAuthorization()
        locManager.startUpdatingLocation()
        arrCoordinates = NSMutableArray()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        guard ARWorldTrackingConfiguration.isSupported else {
            fatalError("""
                ARKit is not available on this device. For apps that require ARKit
                for core functionality, use the `arkit` key in the key in the
                `UIRequiredDeviceCapabilities` section of the Info.plist to prevent
                the app from installing. (If the app can't be installed, this error
                can't be triggered in a production scenario.)
                In apps where AR is an additive feature, use `isSupported` to
                determine whether to show UI for launching AR experiences.
            """) // For details, see https://developer.apple.com/documentation/arkit
        }
        
        // Start the view's AR session with a configuration that uses the rear camera,
        // device position and orientation tracking, and plane detection.
        let configuration = ARWorldTrackingConfiguration()
        configuration.planeDetection = [.horizontal, .vertical]
        sceneView.session.run(configuration)
        
        // Prevent the screen from being dimmed after a while as users will likely
        // have long periods of interaction without touching the screen or buttons.
        UIApplication.shared.isIdleTimerDisabled = true
        
        // Show debug UI to view performance metrics (e.g. frames per second).
        sceneView.showsStatistics = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Pause the view's AR session.
        sceneView.session.pause()
    }
    
    // MARK: - ARSCNViewDelegate
    
    /// - Tag: PlaceARContent
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        if  isStart {
            // Place content only for anchors found by plane detection.
            guard let planeAnchor = anchor as? ARPlaneAnchor else { return }
            //anchortoSave = planeAnchor
            
            // Create a SceneKit plane to visualize the plane anchor using its position and extent.
            let plane = SCNPlane(width: CGFloat(planeAnchor.extent.x), height: CGFloat(planeAnchor.extent.z))
            
            // Saving extent to core data and creating the context of core data
            let context = appDelegate.persistentContainer.viewContext
            let entity = NSEntityDescription.entity(forEntityName: "Coordinates", in: context)
            let newUser = NSManagedObject(entity: entity!, insertInto: context)
            extentz =  CGFloat(planeAnchor.extent.z) //+ extentz
            newUser.setValue(extentz, forKey: "extent_z")
            newUser.setValue(planeAnchor.extent.x, forKey: "extent_x")
            print("-----------------------------")
            print(planeAnchor.extent.z)
            print(planeAnchor.extent.x)
            
            
            let planeNode = SCNNode(geometry: plane)
            
            
            planeNode.simdPosition = float3(planeAnchor.center.x, -0.4, planeAnchor.center.z)
            let vector:SCNVector3 = planeNode.worldPosition
            centerz = CGFloat(vector.z) //+ centerz
            //            print(planeAnchor.center.z)
            //            print(centerz)
            newUser.setValue(planeAnchor.center.x, forKey: "center_x")
            newUser.setValue(centerz, forKey: "center_z")
            print(planeAnchor.center.x)
            print(planeAnchor.center.z)
            
            DispatchQueue.main.async {
                //   self.coordinatesTextView.text = String.init(format: "extent x =%d \n extent z = %d\n center x = 5d\n center z = %d",planeAnchor.extent.x,planeAnchor.extent.z,planeAnchor.center.x,planeAnchor.center.z )
            }
            
            do {
                try context.save()
            } catch {
                print("Failed saving")
            }
            
            // `SCNPlane` is vertically oriented in its local coordinate space, so
            // rotate the plane to match the horizontal orientation of `ARPlaneAnchor`.
            planeNode.eulerAngles.x = -.pi / 2
            
            // Make the plane visualization semitransparent to clearly show real-world placement.
            planeNode.opacity = 0.7
            
            plane.firstMaterial?.diffuse.contents  = UIColor(red: 30.0 / 255.0, green: 150.0 / 255.0, blue: 30.0 / 255.0, alpha: 1)
            
            
            // Add the plane visualization to the ARKit-managed node so that it tracks
            // changes in the plane anchor as plane estimation continues.
            node.addChildNode(planeNode)
        }
    }
    
    /// - Tag: UpdateARContent
    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
        // Update content only for plane anchors and nodes matching the setup created in `renderer(_:didAdd:for:)`.
        guard let planeAnchor = anchor as?  ARPlaneAnchor,
            let planeNode = node.childNodes.first,
            let plane = planeNode.geometry as? SCNPlane
            else { return }
        
        // Plane estimation may shift the center of a plane relative to its anchor's transform.
        planeNode.simdPosition = float3(planeAnchor.center.x, -0.4, planeAnchor.center.z)
        
        // Plane estimation may also extend planes, or remove one plane to merge its extent into another.
        plane.width = CGFloat(0.3)
        plane.height = CGFloat(planeAnchor.extent.z)
    }
    
    // MARK: - ARSessionDelegate
    
    func session(_ session: ARSession, didAdd anchors: [ARAnchor]) {
        guard let frame = session.currentFrame else { return }
        updateSessionInfoLabel(for: frame, trackingState: frame.camera.trackingState)
    }
    
    func session(_ session: ARSession, didRemove anchors: [ARAnchor]) {
        guard let frame = session.currentFrame else { return }
        updateSessionInfoLabel(for: frame, trackingState: frame.camera.trackingState)
    }
    
    func session(_ session: ARSession, cameraDidChangeTrackingState camera: ARCamera) {
        updateSessionInfoLabel(for: session.currentFrame!, trackingState: camera.trackingState)
    }
    
    // MARK: - ARSessionObserver
    
    func sessionWasInterrupted(_ session: ARSession) {
        // Inform the user that the session has been interrupted, for example, by presenting an overlay.
        sessionInfoLabel.text = "Session was interrupted"
    }
    
    func sessionInterruptionEnded(_ session: ARSession) {
        // Reset tracking and/or remove existing anchors if consistent tracking is required.
        sessionInfoLabel.text = "Session interruption ended"
        resetTracking()
    }
    
    func session(_ session: ARSession, didFailWithError error: Error) {
        // Present an error message to the user.
        sessionInfoLabel.text = "Session failed: \(error.localizedDescription)"
        resetTracking()
    }
    
    // MARK: - Private methods
    
    private func updateSessionInfoLabel(for frame: ARFrame, trackingState: ARCamera.TrackingState) {
        // Update the UI to provide feedback on the state of the AR experience.
        let message: String
        
        switch trackingState {
        case .normal where frame.anchors.isEmpty:
            // No planes detected; provide instructions for this app's AR interactions.
            message = "Move the device around to detect horizontal surfaces."
            
        case .notAvailable:
            message = "Tracking unavailable."
            
        case .limited(.excessiveMotion):
            message = "Tracking limited - Move the device more slowly."
            
        case .limited(.insufficientFeatures):
            message = "Tracking limited - Point the device at an area with visible surface detail, or improve lighting conditions."
            
        case .limited(.initializing):
            message = "Initializing AR session."
            
        default:
            // No feedback needed when tracking is normal and planes are visible.
            // (Nor when in unreachable limited-tracking states.)
            message = ""
            
        }
        
        sessionInfoLabel.text = message
        sessionInfoView.isHidden = message.isEmpty
    }
    
    @IBAction func startButtonPressed(_ sender: Any) {
        //        self.resetTracking()
        //        isStart = true
        //        startButton.isUserInteractionEnabled = false
                stopButton.isUserInteractionEnabled = true
        
        if (CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways){
            currentLocation = locManager.location
        
            print(currentLocation.coordinate.latitude)
            print(currentLocation.coordinate.longitude)
            arrCoordinates.add(currentLocation)
        }
    }
    func degreesToRadians(degrees: Double) -> Double {
        return degrees * .pi / 180.0
    }
    
    func radiansToDegrees(radians: Double) -> Double {
        return radians * 180.0 / .pi
    }
    
    func getBearingBetweenTwoPoints1(point1 : CLLocation, point2 : CLLocation) -> Double {
        
        let lat1 = degreesToRadians(degrees: point1.coordinate.latitude)
        let lon1 = degreesToRadians(degrees: point1.coordinate.longitude)
        
        let lat2 = degreesToRadians(degrees: point2.coordinate.latitude)
        let lon2 = degreesToRadians(degrees: point2.coordinate.longitude)
        
        let dLon = lon2 - lon1
        
        let y = sin(dLon) * cos(lat2)
        let x = cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(dLon)
        let radiansBearing = atan2(y, x)
        
        return radiansToDegrees(radians: radiansBearing)
    }
    @IBAction func stopButtonPressed(_ sender: Any) {
        //        sceneView.delegate = nil
        isStart = false
        startButton.isUserInteractionEnabled = false
        stopButton.isUserInteractionEnabled = true
        var height:Float = 0.0
        for i in 1..<arrCoordinates.count {
            let coordinate1 = arrCoordinates.object(at: i-1) as! CLLocation
            let coordinate2 = arrCoordinates.object(at: i) as! CLLocation
            let distanceinMeters = coordinate1.distance(from: coordinate2)
            
            let plane = SCNPlane(width: CGFloat(0.153460693359375), height: CGFloat(distanceinMeters))
            let planeNode = SCNNode(geometry: plane)
            planeNode.simdPosition = float3(0.0, -1.0 ,  Float(Float(-height) - Float(plane.height/2)))
            planeNode.eulerAngles.x = -.pi / 2
            planeNode.opacity = 0.8
            plane.firstMaterial?.diffuse.contents=UIColor .red
            sceneView.scene.rootNode.addChildNode(planeNode)
            height = Float(distanceinMeters)
           //print(getBearingBetweenTwoPoints1(point1: coordinate1, point2: coordinate2))
            let d:Double = getBearingBetweenTwoPoints1(point1: coordinate1, point2: coordinate2)
            print(d)
        }
        
        func locationManager(manager: CLLocationManager, didUpdateHeading: CLHeading){
        
        }
        
       // let coordinate3 = arrCoordinates.object(at: 2) as! CLLocation
        
        
        
//        print("distance in meters = ", distanceinMeters)
//        print("distance in meters = ", distanceInMeters1)
        
        
//        let plane = SCNPlane(width: CGFloat(0.153460693359375), height: CGFloat(distanceinMeters))
//        let planeNode = SCNNode(geometry: plane)
//        planeNode.simdPosition = float3(Float(0.0), -1.0 , Float(-distanceinMeters/2 - 1))
//        planeNode.eulerAngles.x = -.pi / 2
//        planeNode.opacity = 0.7
//        plane.firstMaterial?.diffuse.contents=UIColor .yellow
        
        //-plane.height) - Float(distance2/2
       
        
        
        
        //sceneView.scene.rootNode.addChildNode(planeNode1)

    }
    
    
    
    //    @IBAction func getPath(_ sender: Any){
    //        let context = appDelegate.persistentContainer.viewContext
    //        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Coordinates")
    //        //request.predicate = NSPredicate(format: "age = %@", "12")
    //        request.returnsObjectsAsFaults = false
    //        do {
    //            let result = try context.fetch(request)
    //            var x = 30.0
    //            var y = 150.0
    //            var z = 30.0
    //            for data in result as! [NSManagedObject] {
    //                print("center x")
    //                print(data.value(forKey: "center_x") as! Double)
    //                print("center z")
    //                print(data.value(forKey: "center_z") as! Double)
    //                print("extent z")
    //                print(data.value(forKey: "extent_z") as! Double)
    //                print("extent x")
    //                print(data.value(forKey: "extent_x") as! Double)
    //
    //                let plane = SCNPlane(width: CGFloat(data.value(forKey: "extent_x") as! Double), height: CGFloat(data.value(forKey: "extent_z") as! Double))
    //                //                let context = appDelegate.persistentContainer.viewContext
    //                //                let entity = NSEntityDescription.entity(forEntityName: "Coordinates", in: context)
    //                //                let newUser = NSManagedObject(entity: entity!, insertInto: context)
    //                //                newUser.setValue(planeAnchor.extent.z, forKey: "extent_z")
    //                //                newUser.setValue(planeAnchor.extent.x, forKey: "extent_x")
    //                //
    //
    //                let planeNode = SCNNode(geometry: plane)
    //                planeNode.simdPosition = float3(Float(data.value(forKey: "center_x") as! Double), -0.4, Float(data.value(forKey: "center_z") as! Double))
    //
    //                //                do {
    //                //                    try context.save()
    //                //                } catch {
    //                //                    print("Failed saving")
    //                //                }
    //
    //                // `SCNPlane` is vertically oriented in its local coordinate space, so
    //                // rotate the plane to match the horizontal orientation of `ARPlaneAnchor`.
    //                planeNode.eulerAngles.x = -.pi / 2
    //
    //                // Make the plane visualization semitransparent to clearly show real-world placement.
    //                planeNode.opacity = 0.7
    //                //let node = SCNNode.self
    //                //                if coordinatesTextView.text?.count == 0 {
    //                //                coordinatesTextView.text = "extent x = " + "\((data.value(forKey: "extent_x") as! Double))" + "\n" + "extent z = " + "\((data.value(forKey: "extent_z") as! Double))" + "\n" + "center x = " +  "\((data.value(forKey: "center_x") as! Double))" + "\n" + "center z = " + "\((data.value(forKey: "center_z") as! Double))"
    //                //                }
    //                //                else{
    //                //                coordinatesTextView.text = String.init(format: "%@\n extent x = %d \n extent z = %d\n center x = %d\n center z = %d", coordinatesTextView.text!,(data.value(forKey: "extent_x") as! Double),(data.value(forKey: "extent_z") as! Double), (data.value(forKey: "center_x") as! Double), (data.value(forKey: "center_z") as! Double))
    //                //                }
    ////                print(data.value(forKey: "extent_x")!)
    ////                print(data.value(forKey: "extent_z")!)
    ////                print(data.value(forKey: "center_x")!)
    ////                print(data.value(forKey: "center_z")!)
    //
    //                sceneView.scene.rootNode.addChildNode(planeNode)
    //
    //                plane.firstMaterial?.diffuse.contents  = UIColor(red: CGFloat(x / 255.0), green: CGFloat(y / 255.0), blue: CGFloat(z / 255.0), alpha: 1)
    //                x = x + 10
    //                y = y - 30
    //                z = z + 30
    //
    //              //  let node = sceneView.node(for: anchortoSave)
    //              //  node?.addChildNode(planeNode)
    //
    //            }
    //
    //        } catch {
    //
    //            print("Failed")
    //        }
    //    }
    
    @IBAction func getPath(_ sender: Any){
        let plane = SCNPlane(width: CGFloat(0.063460693359375), height: CGFloat(0.173185348510742))
        let planeNode = SCNNode(geometry: plane)
        planeNode.simdPosition = float3(Float(0.0264189541339874),0, Float(0.0172050558030605))
        planeNode.eulerAngles.x = -.pi / 2
        planeNode.opacity = 0.7
        plane.firstMaterial?.diffuse.contents=UIColor .yellow
        
        
        let plane1 = SCNPlane(width: CGFloat(0.173185348510742), height: CGFloat(0.023460693359375))
        let planeNode1 = SCNNode(geometry: plane1)
        planeNode1.simdPosition = float3(Float(0.1244189541339878),0, Float(0.0920558030605))
        planeNode1.eulerAngles.x = -.pi / 2
        planeNode1.opacity = 0.6
        plane1.firstMaterial?.diffuse.contents=UIColor .red
        
        
        
        let plane2 = SCNPlane(width: CGFloat(0.023460693359375), height: CGFloat(0.173185348510742))
        let planeNode2 = SCNNode(geometry: plane2)
        planeNode2.simdPosition = float3(Float(0.22189541349998),0, Float(0.167002030605))
        planeNode2.eulerAngles.x = -.pi / 2
        planeNode2.opacity = 0.6
        plane2.firstMaterial?.diffuse.contents=UIColor .green
        
        
        sceneView.scene.rootNode.addChildNode(planeNode2)
        sceneView.scene.rootNode.addChildNode(planeNode1)
        sceneView.scene.rootNode.addChildNode(planeNode)
    }
    private func resetTracking() {
        let configuration = ARWorldTrackingConfiguration()
        configuration.planeDetection = .horizontal
        sceneView.session.run(configuration, options: [.resetTracking, .removeExistingAnchors])
    }
}
